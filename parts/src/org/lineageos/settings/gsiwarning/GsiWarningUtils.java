/*
 * Copyright (C) 2023-2024 The LineageOS Project
 * SPDX-License-Identifier: Apache-2.0
 */

package org.lineageos.settings.gsiwarning;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;

import org.lineageos.settings.R;

public class GsiWarningUtils {

    private static final String TAG = "GsiWarningUtils";
    private static final String PREF_FIRST_BOOT = "first_boot";
    private static final String CHANNEL_ID = "GSI_WARNING_CHANNEL";
    private static final int NOTIFICATION_ID = 1;

    public static void startService(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        if (isFirstBoot(prefs)) {
            showNotification(context);
            prefs.edit().putBoolean(PREF_FIRST_BOOT, false).apply();
        }
    }

    private static void showNotification(Context context) {
        createNotificationChannel(context);

        Intent intent = new Intent(context, AlertDialogActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
               PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_warning)
                .setContentTitle(context.getString(R.string.gsi_warning_title))
                .setContentText(context.getString(R.string.gsi_warning_message))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.gsi_warning_title);
            String description = context.getString(R.string.gsi_warning_message);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private static boolean isFirstBoot(SharedPreferences prefs) {
        return prefs.getBoolean(PREF_FIRST_BOOT, true);
    }
}
